use test_one::up;
use test_one::up::upstore;


#[async_std::main]
async fn main() {
    // load the datastore, and helper methods
    let store = upstore::load("./store.db").await;
    // load the distributor
    let _ = up::setup(store).await;

    // keep running, keep running
    loop {
        std::future::pending::<()>().await;
    };
}
