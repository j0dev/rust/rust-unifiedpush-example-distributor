use zbus::Connection;
use zbus::dbus_interface;
use zbus::fdo::Error;

use crate::up::distributor;
use crate::up::upstore;
use crate::up::models;

use log::{error, warn, info, debug, trace};

pub struct Distributor {
    conn: &'static Connection,
    store: upstore::UpStore,
}

#[dbus_interface(name = "org.unifiedpush.Distributor1")]
impl Distributor {
    
    #[dbus_interface(out_args("registrationResult", "registrationResultReason"))]
    async fn register(&self, serviceName: &str, token: &str, description: &str) -> Result<(String, String), Error> {
        debug!("[UP distributor] register: Received request for '{}' with token: '{}' and description: '{}'", serviceName, token, description);

        // check if we have a registration already
        if let Some(reg) = self.store.get_registration(token.to_string()).await {
            // check if the service_name is the same
            if reg.app_id != serviceName {
                // Giving a generic message, as to not try and give away that the token is in use by another one, as with the token, you can force unregister
                return Ok(("REGISTRATION_FAILED".to_string(), String::from("The given token value is not allowed!")))
            }
            // existing registration
            debug!("[UP distributor] register: Existing registration found: {:?}", reg);
            // send endpoint
            send_endpoint(self.conn, reg).await
        } else {
            // new registration
            debug!("[UP distributor] register: New registration");
            
            // generate new endpoint and topic
            let topic = self.store.generate_topic().await;
            let endpoint = self.store.construct_endpoint(topic.clone()).await;
            debug!("[UP distributor] register: Generated topic '{}' and endpoint '{}'", topic, endpoint);
            
            // save new endpoint
            let registration = models::Registration{
                id: -1,
                app_id: String::from(serviceName),
                token: String::from(token),
                description: Some(String::from(description)),
                endpoint: endpoint,
            };
            self.store.add_registration(registration.clone()).await;
            
            // IMPLEMENTATION: ensure the system listens for new notifications on the topic/endpoint
            
            // send endpoint
            send_endpoint(self.conn, registration).await
        }

        Ok(("REGISTRATION_SUCCEEDED".to_string(), String::new()))
    }

    async fn unregister(&self, token: &str) -> Result<(), Error> {
        // get the current registration
        if let Some(reg) = self.store.get_registration(token.to_string()).await {
            // remove the registration
            self.store.remove_registration(reg.clone()).await;

            // IMPLEMENTATION: ensure the system stops listening for new notifications on the topic/endpoint

            // send a confirmation to the Connector
            send_unregister(self.conn, reg).await;
            return Ok(())
        } else {
            // none found, oh well, doing nothing
            return Ok(())
        }
    }

    // FAKE NOTIFICATION RECEIVER, USE THIS FUNCTION TO SEND NOTIFICATIONS TO CONNECTORS TO TEST NOTIFICATIONS
    // THIS FUNCTION IS NOT PART OF THE SPEC AND SHOULD NOT BE IMPLEMENTED
    async fn fake_push(&self, endpoint: &str, message: &[u8]) -> Result<(String, String), Error> {
        let reg = self.store.get_registration_by_endpoint(endpoint.to_string()).await;
        if let Some(reg) = reg {
            // Send notification
            send_message(self.conn, reg, message).await;

            return Ok((
                String::from("SUCCESS"),
                String::from("Message has been send, though we have no way to check if it is received, or the application listening / activated")
            ))
        } else {
            return Ok((
                String::from("FAIL"),
                String::from("Endpoint registration not found!")
            ))
        }
    }
    async fn fake_push_str(&self, endpoint: &str, message: &str) -> Result<(String, String), Error> {
        let reg = self.store.get_registration_by_endpoint(endpoint.to_string()).await;
        if let Some(reg) = reg {
            // Send notification
            send_message(self.conn, reg, message.as_bytes()).await;

            return Ok((
                String::from("SUCCESS"),
                String::from("Message has been send, though we have no way to check if it is received, or the application listening / activated")
            ))
        } else {
            return Ok((
                String::from("FAIL"),
                String::from("Endpoint registration not found!")
            ))
        }
    }
}

// new initializes a Distributor dbus interface instance to register to the object_server
pub fn new(conn: &'static Connection, store: upstore::UpStore) -> Distributor {
    distributor::Distributor{
        conn,
        store,
    }
}

// send_endpoint sends the new endpoint, where UnifiedPush messages should be send to by an application server, to the registering Connector application
pub async fn send_endpoint(conn: &Connection, registration: models::Registration) {
    let result = conn.send_message(zbus::Message::method(
        conn.unique_name(),
        Some(registration.app_id),
        "/org/unifiedpush/Connector",
        Some("org.unifiedpush.Connector1"),
        "NewEndpoint",
        &(registration.token, registration.endpoint)).unwrap()).await;
    if let Err(e) = result {
        println!("Failed to send new endpoint: {:?}", e);
    }
}

// send_unregister sends the unregister confirmation to the Connector application
pub async fn send_unregister(conn: &Connection, registration: models::Registration) {
    let result = conn.send_message(zbus::Message::method(
        conn.unique_name(),
        Some(registration.app_id),
        "/org/unifiedpush.Connector1",
        Some("org.unifiedpush.Connector1"),
        "Unregistered",
        &(registration.token)).unwrap()).await;
    if let Err(e) = result {
        println!("Failed to send unregister confirmation: {:?}", e);
    }
}

// send_message sends a UnifiedPush message to a Connector application
pub async fn send_message(conn: &Connection, registration: models::Registration, message: &[u8]) {
    // send the message
    let result = conn.send_message(zbus::Message::method(
        conn.unique_name(),
        Some(registration.app_id),
        "/org/unifiedpush/Connector",
        Some("org.unifiedpush.Connector1"),
        "Message",
        &(registration.token, message, String::new())).unwrap()).await;

    // Note: Errors are not triggered if the message does not have a destination, it will just send, and go nowhere
    if let Err(e) = result {
        println!("Failed to send message: {:?}", e);
    }
}
