#[derive(Clone, Debug)]
pub struct Registration {
    pub id: i64,
    pub app_id: String,
    pub token: String,
    pub description: Option<String>,
    pub endpoint: String,
}