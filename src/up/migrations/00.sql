CREATE TABLE IF NOT EXISTS "properties" (
    "key"    TEXT NOT NULL,
    "value"  TEXT NOT NULL,
    PRIMARY KEY ("key")
);

CREATE TABLE IF NOT EXISTS "upstore" (
    "id"           INTEGER NOT NULL,
    "app_id"       TEXT NOT NULL,
    "token"        TEXT NOT NULL,
    "description"  TEXT,
    "endpoint"     TEXT NOT NULL,
    PRIMARY KEY("id" AUTOINCREMENT)
);

-- INSERT INTO "properties" VALUES ("schema", "1");