use zbus::Connection;

pub mod distributor;
pub mod upstore;
pub mod models;

pub async fn setup(store: upstore::UpStore) {
    // create connection, Box it and leak it, we want this to survive the entire runtime anyway
    let dbus_conn = Box::new(Connection::session().await.unwrap());
    let leaked_conn: &'static mut Connection = Box::leak(dbus_conn);

    // setup our distributor handler
    let distributor = distributor::new(leaked_conn, store);
    // bind our distributor to dbus
    let os = leaked_conn.object_server();
    let _distributor = os.at("/org/unifiedpush/Distributor", distributor).await.unwrap();

    // setup dbus name to expose everything under
    let _a = leaked_conn.request_name("org.unifiedpush.Distributor.Notify").await.unwrap();

    
    // TODO: stay running in another thread with this still in context, or dbus will just drop again
    //       We might see if it will stay without the internal executor automatically running in another thread
    // Update: this will stay working, most likely because the dbus_conn is leaked statically
    // Also I this was added when I was still using the blocking ConnectionBuilder,
    //  but I need the connection beforehand as it is needed inside the distributor
    
    // loop {
    //     std::future::pending::<()>().await;
    // }
}
