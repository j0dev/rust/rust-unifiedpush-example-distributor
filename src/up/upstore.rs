use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;
use tokio_rusqlite::{Connection, params};

use crate::up::models::Registration;

#[derive(Clone, Debug)]
pub struct UpStore {
    conn: Connection,
    server: String,
    topic_prefix: String
}

impl UpStore {
    // get_registration tries to retrieve a registration from the store
    pub async fn get_registration(&self, token: String) -> Option<Registration> {
        let ans = self.conn.call(move |conn| {
            let mut stmt = conn.prepare("
                SELECT
                id,
                app_id, token, description,
                endpoint
                FROM upstore
                WHERE token=?;
            ")?;

            let ans = stmt.query_row(params![token], |row| {
                Ok(Registration{
                    id: row.get(0)?,
                    app_id: row.get(1)?,
                    token: row.get(2)?,
                    description: row.get(3)?,
                    endpoint: row.get(4)?,
                })
            });
            if let Ok(ans) = ans {
                return Ok(Some(ans))
            } else {
                return Ok(None)
            }
        }).await;
        return ans.unwrap()
    }

    // get_registration_by_endpoint tries to retrieve a registration using an endpoint from the store
    pub async fn get_registration_by_endpoint(&self, endpoint: String) -> Option<Registration> {
        let ans = self.conn.call(move |conn| {
            let mut stmt = conn.prepare("
                SELECT
                id,
                app_id, token, description,
                endpoint
                FROM upstore
                WHERE endpoint=?;
            ")?;

            let ans = stmt.query_row(params![endpoint], |row| {
                Ok(Registration{
                    id: row.get(0)?,
                    app_id: row.get(1)?,
                    token: row.get(2)?,
                    description: row.get(3)?,
                    endpoint: row.get(4)?,
                })
            });
            if let Ok(ans) = ans {
                return Ok(Some(ans))
            } else {
                return Ok(None)
            }
        }).await;
        return ans.unwrap();
    }

    // add_registration adds a new registration to the store
    pub async fn add_registration(&self, registration: Registration) {
        let _ret = self.conn.call(move |conn| {
            let mut stmt = conn.prepare("
                INSERT INTO upstore (
                    app_id, token, description,
                    endpoint
                ) VALUES (?, ?, ?, ?);
            ")?;
            let ret = stmt.execute(params![registration.app_id, registration.token, registration.description, registration.endpoint]);
            Ok(ret.unwrap())
        }).await;
    }

    // remove_registration removes a registration from the store
    pub async fn remove_registration(&self, registration: Registration) {
        let _ret = self.conn.call(move |conn| {
            let mut stmt = conn.prepare("
                DELETE FROM upstore
                WHERE id=?;
            ")?;

            let ans = stmt.execute(params![registration.id]);
            if let Ok(_ans) = ans {
                Ok(true)
            } else {
                Ok(false)
            }
        }).await;
    }

    // generate_topic generates a new unique and random topic name
    pub async fn generate_topic(&self) -> String {
        let mut ran: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(12)
            .map(char::from)
            .collect();
        ran.insert_str(0, "up");
        ran.insert_str(2, &self.topic_prefix);
        ran
    }

    // construct_endpoint constructs the full endpoint url from a given topic name
    // Note: this function is currently setup for generating the url format for ntfy servers
    //  When implementing this, check that it returns the urls that you need for your server-side distributor
    pub async fn construct_endpoint(&self, topic: String) -> String {
        let mut endpoint = String::from(topic);
        endpoint.insert_str(0, "/");
        endpoint.insert_str(0, &self.server);
        endpoint.push_str("?up=1");
        endpoint
    }
}
// load constructs and loads the store from a given database storage location
pub async fn load(db_path: &str) -> UpStore {
    let conn = Connection::open(db_path).await.unwrap();

    {
        let _ = conn.call(|conn| {
            conn.execute_batch(
            "PRAGMA foreign_keys = ON;
             PRAGMA journal_mode = wal;",
            ).unwrap();
            Ok(())
        }).await;

        let _ = conn.call(|conn| {
            conn.execute_batch(include_str!("./migrations/00.sql")).unwrap();
            Ok(())
        }).await;

    }

    let store = UpStore{
        conn: conn,
        server: String::from("https://ntfy.j0dev.nl"),
        topic_prefix: String::from(""),
    };

    store
}
