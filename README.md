UnifiedPush Fake Distributor Proof of Concept for linux
======

This repository contains a **Fake** UnifiedPush distributor as a proof of concept to function as a guide to implementing it in Notify and as a learning exercise for the author.


## Progress
- [x] Persistent storage, using sqlite datastore
      Note: only what is needed by the other sections is implemented
- [x] Async dbus setup
- [x] Distributor register
  - [x] Registration state storage
  - [x] Endpoint notification
- [x] Distributor unregister
- [x] Connector message sending
  - [x] Message sending

- [ ] Documentation

- [x] Fake message sending (to have a fully working distributor to test with (with exception to automated delivery))
  - [x] Basic topic handling
  - [x] Actual sending logic, see 'Connector message sending'


## Test usage
This distributor is actually functional. With one exception: It does not receive notifications from a server side distributor component.

In order to run this correctly in addition to the build-in migrations, there are 2 rows that need to be added in the sqlite database.

Open the store.db with an sqlite editor and insert the 2 following values in the properties table (this table is used as a key => value store)

    INSERT INTO properties (key, value) VALUES
    ("server", "ntfy.sh"),
    ("topic_prefix", "");

You should replace the server url with your own instance, though this is only used for constructing endpoint urls as this distributor does not connect to any server-side distributor.  
DO NOT ABUSE THIS AND LET APPS SEND NOTIFICATIONS TO TOPICS THAT ARE NOT READ AND FORWARDED BY APP SERVERS!


## Things to note
This was initially build with integration into [Notify](https://github.com/ranfdev/Notify) in mind, so keep that in mind.

The sqlite integration is changed from _rusqlite_ to _tokio_rusqlite_  
This change was done as the D-bus library uses a separate thread to handle requests.  
_tokio_rusqlite_ provides asynchronous access to `rusqlite::Connection`


## dependencies
- rusqlite / tokio_rusqlite: sqlite datastore
- zbus: Provides the D-bus communication layer


## Future work
- [ ] Fix unifiedpush golang & C connector library
  - [ ] WIP code update
  - [ ] PR merge ([issue](https://github.com/UnifiedPush/go_dbus_connector/issues/8) & [merge request](https://github.com/UnifiedPush/go_dbus_connector/pull/9))
  - [ ] Documentation update
- [ ] Spec doc update: add unregister confirmation sending to distributor unregister call
- [ ] Fix/Update unifiedpush gotify distributor ([repo](https://github.com/UnifiedPush/gotify-dbus-rust))
- [ ] Rust Connector library
